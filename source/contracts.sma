#include <amxmodx>
#include <reapi>

#define PLUGIN_NAME		"Contracts"
#define PLUGIN_VERSION	"1.0"
#define PLUGIN_AUTHOR	"StockX"

enum _:
{
	Plural,
	Singular,
	Nominative
};

enum _:Structure
{
	Client_Index,

	Target_Index,
	Target_Name[MAX_NAME_LENGTH],
	Target_Reward,
	Target_Time,

	bool:Is_Target,
	bool:Waiting_Completion,

	Time_List,
	Reward_List
};

new Trie:trie_messages;

new Array:array_contract;
new Array:array_time;
new Array:array_reward;

new contract_data[Structure];

new contracts_num;
new player[MAX_CLIENTS + 1][Structure];

public plugin_init()
{
	register_plugin(PLUGIN_NAME, PLUGIN_VERSION, PLUGIN_AUTHOR);

	RegisterHookChain(RG_CBasePlayer_Killed, "CBasePlayer_Killed_Post", 1);
	RegisterHookChain(RG_CSGameRules_RestartRound, "RG_CSGameRules_RestartRound_Pre", 0);

	trie_messages = TrieCreate();
	array_contract = ArrayCreate(Structure);
	array_reward = ArrayCreate(Structure);
	array_time = ArrayCreate(Structure);

	new INIParser:parser = INI_CreateParser();
	INI_SetReaders(parser, "Parser_KeyValue", "Parser_NewSection");
	INI_ParseFile(parser, "addons/amxmodx/configs/contracts.ini");
	INI_DestroyParser(parser);
}

public Parser_NewSection()
	return true;

public Parser_KeyValue(handle, const key[], const value[])
{
	new value_new[256];
	formatex(value_new, charsmax(value_new), value);

	replace_string(value_new, charsmax(value_new), "!n", "^1");
	replace_string(value_new, charsmax(value_new), "!g", "^4");
	replace_string(value_new, charsmax(value_new), "!t", "^3");

	if(!strcmp(key, "MSG_PLAYER_LEAVE"))
		TrieSetString(trie_messages, "MSG_PLAYER_LEAVE", value_new);
	else if(!strcmp(key, "MSG_TIMELEFT"))
		TrieSetString(trie_messages, "MSG_TIMELEFT", value_new);
	else if(!strcmp(key, "MSG_CONTRACT_COMPLETED"))
		TrieSetString(trie_messages, "MSG_CONTRACT_COMPLETED", value_new);
	else if(!strcmp(key, "MSG_DISCONNECTED"))
		TrieSetString(trie_messages, "MSG_DISCONNECTED", value_new);
	else if(!strcmp(key, "MSG_SELECT_PLAYER"))
		TrieSetString(trie_messages, "MSG_SELECT_PLAYER", value_new);
	else if(!strcmp(key, "MSG_SELECT_REWARD"))
		TrieSetString(trie_messages, "MSG_SELECT_REWARD", value_new);
	else if(!strcmp(key, "MSG_SELECT_TIME"))
		TrieSetString(trie_messages, "MSG_SELECT_TIME", value_new);
	else if(!strcmp(key, "MSG_CONTRACT_ACTIVATED"))
		TrieSetString(trie_messages, "MSG_CONTRACT_ACTIVATED", value_new);
	else if(!strcmp(key, "MSG_NOT_MONEY"))
		TrieSetString(trie_messages, "MSG_NOT_MONEY", value_new);
	else if(!strcmp(key, "MSG_WAITING"))
		TrieSetString(trie_messages, "MSG_WAITING", value_new);

	if(!strcmp(key, "COMMANDS"))
	{
		new left_cmd[MAX_FMT_LENGTH], right_cmd[MAX_FMT_LENGTH];

		for(new i; i < strlen(value_new); i++)
		{
			if(value_new[i] == ' ')
			{
				strtok2(value_new, left_cmd, charsmax(left_cmd), right_cmd, charsmax(right_cmd), ' ');
				replace(value_new, charsmax(value_new), fmt("%s ", left_cmd), "");

				if(left_cmd[0] != EOS)
				{
					register_clcmd(left_cmd, "MainMenu");
					register_clcmd(fmt("say %s", left_cmd), "MainMenu");
					register_clcmd(fmt("say_team %s", left_cmd), "MainMenu");
				}

				i -= (strlen(left_cmd) - 1);
			}
		}

		if(right_cmd[0] != EOS)
		{
			register_clcmd(right_cmd, "MainMenu");
			register_clcmd(fmt("say %s", right_cmd), "MainMenu");
			register_clcmd(fmt("say_team %s", right_cmd), "MainMenu");
		}
	}

	if(!strcmp(key, "TIME_LIST"))
	{
		new left_cmd[MAX_FMT_LENGTH], right_cmd[MAX_FMT_LENGTH];

		for(new i; i < strlen(value_new); i++)
		{
			if(value_new[i] == ' ')
			{
				strtok2(value_new, left_cmd, charsmax(left_cmd), right_cmd, charsmax(right_cmd), ' ');
				replace(value_new, charsmax(value_new), fmt("%s ", left_cmd), "");

				if(left_cmd[0] != EOS)
				{
					contract_data[Time_List] = str_to_num(left_cmd);
					ArrayPushArray(array_time, contract_data);
				}

				i -= strlen(left_cmd);
			}
		}

		if(right_cmd[0] != EOS)
		{
			contract_data[Time_List] = str_to_num(right_cmd);
			ArrayPushArray(array_time, contract_data);
		}
	}

	if(!strcmp(key, "REWARD_LIST"))
	{
		new left_cmd[MAX_FMT_LENGTH], right_cmd[MAX_FMT_LENGTH];

		for(new i; i < strlen(value_new); i++)
		{
			if(value_new[i] == ' ')
			{
				strtok2(value_new, left_cmd, charsmax(left_cmd), right_cmd, charsmax(right_cmd), ' ');
				replace(value_new, charsmax(value_new), fmt("%s ", left_cmd), "");

				if(left_cmd[0] != EOS)
				{
					contract_data[Reward_List] = str_to_num(left_cmd);
					ArrayPushArray(array_reward, contract_data);
				}

				i -= strlen(left_cmd);
			}
		}

		if(right_cmd[0] != EOS)
		{
			contract_data[Reward_List] = str_to_num(right_cmd);
			ArrayPushArray(array_reward, contract_data);
		}
	}

	return true;
}

public client_putinserver(id)
	ClearData(id, .is_connect = true);

public client_disconnected(id)
{
	if(player[id][Waiting_Completion])
	{
		ClearData(id, player[id][Client_Index]);
		return;
	}

	if(!player[id][Is_Target])
		return;

	new client_id = player[id][Client_Index];

	if(is_user_connected(client_id))
	{
		rg_add_account(client_id, player[client_id][Target_Reward]);

		new value[256];
		TrieGetString(trie_messages, "MSG_PLAYER_LEAVE", value, charsmax(value));
		replace_string(value, charsmax(value), "`target`", player[client_id][Target_Name]);

		client_print_color(client_id, print_team_default, value);

		ClearData(client_id, id);
	}
}

public RG_CSGameRules_RestartRound_Pre()
{
	if(get_member_game(m_bCompleteReset))
		return HC_CONTINUE;

	for(new item; item < ArraySize(array_contract); item++)
	{
		ArrayGetArray(array_contract, item, contract_data);

		contract_data[Target_Time]--;
		ArraySetArray(array_contract, item, contract_data);

		if(contract_data[Target_Time] <= 0)
		{
			rg_add_account(contract_data[Client_Index], contract_data[Target_Reward]);

			new value[256];
			TrieGetString(trie_messages, "MSG_TIMELEFT", value, charsmax(value));
			replace_string(value, charsmax(value), "`target`", contract_data[Target_Name]);

			client_print_color(contract_data[Client_Index], print_team_default, value);

			ClearData(contract_data[Client_Index], contract_data[Target_Index]);
		}
	}

	return HC_CONTINUE;
}

public CBasePlayer_Killed_Post(const victim, killer)
{
	if(victim == killer)
		return HC_CONTINUE;

	if(victim < 1 || victim > MaxClients)
		return HC_CONTINUE;

	if(killer < 1 || killer > MaxClients)
		return HC_CONTINUE;

	if(player[victim][Is_Target])
	{
		new client_id = player[victim][Client_Index];

		new killer_name[MAX_NAME_LENGTH];
		get_user_name(killer, killer_name, charsmax(killer_name));

		new value[256];
		TrieGetString(trie_messages, "MSG_CONTRACT_COMPLETED", value, charsmax(value));
		replace_string(value, charsmax(value), "`target`", player[client_id][Target_Name]);
		replace_string(value, charsmax(value), "`reward`", fmt("%d", player[client_id][Target_Reward]));
		replace_string(value, charsmax(value), "`killer`", killer_name);

		client_print_color(0, print_team_default, value);

		rg_add_account(killer, player[client_id][Target_Reward]);

		ClearData(client_id, victim);
	}

	return HC_CONTINUE;
}

public MainMenu(id)
{
	new menu = menu_create("Контракт на убийство", "Global_Handler");
	new info[] = "main";

	menu_additem(menu, fmt("Отправить заказ \d%s^n", player[id][Waiting_Completion] ? "[ожидайте выполнения]" : ""), info);

	menu_additem(menu, fmt("Игрок: \r%s", player[id][Target_Name]), info);
	menu_additem(menu, fmt("Награда: \r$%d", player[id][Target_Reward]), info);
	menu_additem(menu, fmt("Время выполнения: \r%d %s^n", player[id][Target_Time], get_numerical_noun_form(player[id][Target_Time]) == Nominative ? "раунд" : get_numerical_noun_form(player[id][Target_Time]) == Singular ? "раунда" : "раундов"), info);

	menu_additem(menu, fmt("Посмотреть список заказов \y[\r%d\y]", contracts_num), info);

	menu_setprop(menu, MPROP_EXITNAME, "Выход");
	menu_display(id, menu);

	return PLUGIN_HANDLED;
}

public Global_Handler(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		menu_destroy(menu);
		return PLUGIN_HANDLED;
	}

	new access, info[8], name[MAX_NAME_LENGTH], callback;
	menu_item_getinfo(menu, item, access, info, charsmax(info), name, charsmax(name), callback);

	new target_id = player[id][Target_Index];
	new target_reward = player[id][Target_Reward];

	if(!strcmp(info, "main"))
	{
		switch(item)
		{
			case 0:
			{
				if(!CheckCompletion(id, menu))
					return PLUGIN_HANDLED;

				if(!is_user_connected(target_id))
				{
					new value[256];
					TrieGetString(trie_messages, "MSG_DISCONNECTED", value, charsmax(value));
					client_print_color(id, print_team_default, value);

					ClearData(id, target_id);
					contracts_num++;

					menu_destroy(menu);
					MainMenu(id);

					return PLUGIN_HANDLED;
				}

				if(!player[id][Target_Name][0])
				{
					new value[256];
					TrieGetString(trie_messages, "MSG_SELECT_PLAYER", value, charsmax(value));
					client_print_color(id, print_team_default, value);

					menu_destroy(menu);
					MainMenu(id);

					return PLUGIN_HANDLED;
				}
				if(!target_reward)
				{
					new value[256];
					TrieGetString(trie_messages, "MSG_SELECT_REWARD", value, charsmax(value));
					client_print_color(id, print_team_default, value);

					menu_destroy(menu);
					MainMenu(id);

					return PLUGIN_HANDLED;
				}
				if(!player[id][Target_Time])
				{
					new value[256];
					TrieGetString(trie_messages, "MSG_SELECT_TIME", value, charsmax(value));
					client_print_color(id, print_team_default, value);
					
					menu_destroy(menu);
					MainMenu(id);

					return PLUGIN_HANDLED;
				}

				contracts_num++;
				player[target_id][Client_Index] = contract_data[Client_Index] = id;
				player[target_id][Is_Target] = player[id][Waiting_Completion] = contract_data[Is_Target] = true;

				rg_add_account(id, -target_reward);

				contract_data[Target_Index] = target_id;
				contract_data[Target_Time] = player[id][Target_Time];
				contract_data[Target_Reward] = target_reward;

				copy(contract_data[Target_Name], charsmax(contract_data[Target_Name]), player[id][Target_Name]);

				ArrayPushArray(array_contract, contract_data);

				new value[256];
				TrieGetString(trie_messages, "MSG_CONTRACT_ACTIVATED", value, charsmax(value));
				replace_string(value, charsmax(value), "`target`", contract_data[Target_Name]);
				replace_string(value, charsmax(value), "`reward`", fmt("%d", contract_data[Target_Reward]));

				client_print_color(0, print_team_default, value);
			}
			case 1:
			{
				if(!CheckCompletion(id, menu))
					return PLUGIN_HANDLED;
				PlayersMenu(id);
			}
			case 2:
			{
				if(!CheckCompletion(id, menu))
					return PLUGIN_HANDLED;
				RewardsMenu(id);
			}
			case 3:
			{
				if(!CheckCompletion(id, menu))
					return PLUGIN_HANDLED;
				TimeMenu(id);
			}
			case 4: ContractsListMenu(id);
		}
	}
	else if(!strcmp(info, "players"))
	{
		player[id][Target_Index] = find_player_ex(FindPlayer_MatchName, name);
		copy(player[id][Target_Name], charsmax(player[][Target_Name]), name);

		menu_destroy(menu);
		MainMenu(id);

		return PLUGIN_HANDLED;
	}
	else if(!strcmp(info, "rewards"))
	{
		replace_string(name, charsmax(name), "$", "");

		if(get_member(id, m_iAccount) >= str_to_num(name))
			player[id][Target_Reward] = str_to_num(name);
		else
		{
			new value[256];
			TrieGetString(trie_messages, "MSG_NOT_MONEY", value, charsmax(value));
			client_print_color(id, print_team_default, value);
		}

		menu_destroy(menu);
		MainMenu(id);

		return PLUGIN_HANDLED;
	}
	else if(!strcmp(info, "time"))
	{
		if(item == MENU_EXIT || item == MENU_BACK || item == MENU_MORE)
			return PLUGIN_HANDLED;

		ArrayGetArray(array_time, item, contract_data);

		player[id][Target_Time] = contract_data[Time_List];

		menu_destroy(menu);
		MainMenu(id);

		return PLUGIN_HANDLED;
	}
	else if(info[0] == 'l' && info[1] != EOS)
	{
		ArrayGetArray(array_contract, info[1], contract_data);

		if(id == contract_data[Client_Index])
		{
			client_print_color(id, print_team_default, "[Contracts] Вы не можете принять свой же заказ.");
			return PLUGIN_HANDLED;
		}
		else if(id == contract_data[Target_Index])
		{
			client_print_color(id, print_team_default, "[Contracts] Вы не можете принять заказ на самого себя.");
			return PLUGIN_HANDLED;
		}
	}

	menu_destroy(menu);
	return PLUGIN_HANDLED;
}

public PlayersMenu(id)
{
	new menu = menu_create("Выберите игрока", "Global_Handler");
	new info[] = "players";

	new players[MAX_CLIENTS], players_num;
	get_players(players, players_num);

	for(new i, player_id, name[MAX_NAME_LENGTH]; i < players_num; i++)
	{
		player_id = players[i];

		if(player_id == id)
			continue;

		get_user_name(player_id, name, charsmax(name));
		menu_additem(menu, name, info);
	}

	menu_setprop(menu, MPROP_NEXTNAME, "Далее");
	menu_setprop(menu, MPROP_BACKNAME, "Назад");
	menu_setprop(menu, MPROP_EXITNAME, "Выход");

	menu_display(id, menu);

	return PLUGIN_HANDLED;
}

public RewardsMenu(id)
{
	new menu = menu_create("Выберите награду", "Global_Handler");
	new info[] = "rewards";

	for(new item; item < ArraySize(array_reward); item++)
	{
		ArrayGetArray(array_reward, item, contract_data);
		menu_additem(menu, fmt("$%d", contract_data[Reward_List]), info);
	}

	menu_setprop(menu, MPROP_EXITNAME, "Выход");
	menu_display(id, menu);

	return PLUGIN_HANDLED;
}

public TimeMenu(id)
{
	new menu = menu_create("Время выполнения", "Global_Handler");
	new info[] = "time";

	for(new item; item < ArraySize(array_time); item++)
	{
		ArrayGetArray(array_time, item, contract_data);
		menu_additem(menu, fmt("%d %s", contract_data[Time_List], get_numerical_noun_form(contract_data[Time_List]) == Nominative ? "раунд" : get_numerical_noun_form(contract_data[Time_List]) == Singular ? "раунда" : "раундов"), info);
	}

	menu_setprop(menu, MPROP_EXITNAME, "Выход");
	menu_display(id, menu);

	return PLUGIN_HANDLED;
}

public ContractsListMenu(id)
{
	new menu = menu_create("Список контрактов", "Global_Handler");
	new info[3];
	info[0] = 'l';

	for(new item; item < ArraySize(array_contract); item++)
	{
		ArrayGetArray(array_contract, item, contract_data);

		info[1] = item;

		menu_additem(menu, fmt("Игрок: \r%s \d[\wНаграда: \r%d \d| \wВремя: \r%d\d]", contract_data[Target_Name], contract_data[Target_Reward], contract_data[Target_Time]), info);
	}

	menu_setprop(menu, MPROP_EXITNAME, "Выход");
	menu_display(id, menu);

	return PLUGIN_HANDLED;
}

stock ClearData(client_id = 0, target_id = 0, is_connect = false)
{
	if(is_connect)
	{
		arrayset(player[client_id], 0, sizeof(player[]));
		return;
	}

	if(target_id)
	{
		arrayset(player[target_id], 0, sizeof(player[]));

		for(new item; item < ArraySize(array_contract); item++)
		{
			ArrayGetArray(array_contract, item, contract_data);

			if(target_id == contract_data[Target_Index])
			{
				ArrayDeleteItem(array_contract, item);
				break;
			}
		}
	}

	if(client_id)
	{
		arrayset(player[client_id], 0, sizeof(player[]));

		for(new item; item < ArraySize(array_contract); item++)
		{
			ArrayGetArray(array_contract, item, contract_data);

			if(client_id == contract_data[Client_Index])
			{
				ArrayDeleteItem(array_contract, item);
				break;
			}
		}
	}

	contracts_num--;
}

public plugin_end()
{
	ArrayDestroy(array_contract);
	ArrayDestroy(array_reward);
	ArrayDestroy(array_time);
	TrieDestroy(trie_messages);
}

stock get_numerical_noun_form(iNum)
{
	if(iNum > 10 && ((iNum % 100) / 10) == 1)
		return Plural;

	switch(iNum % 10)
	{
		case 1: return Nominative;
		case 2, 3, 4: return Singular;
	}

	return Plural;
}

stock CheckCompletion(id, menu)
{
	if(player[id][Waiting_Completion])
	{
		new value[256];
		TrieGetString(trie_messages, "MSG_WAITING", value, charsmax(value));
		client_print_color(id, print_team_default, value);

		menu_destroy(menu);
		MainMenu(id);

		return 0;
	}

	return 1;
}